{ pkgs ? import ./fetch-nixpkgs.nix {} }:

with pkgs;
with dockerTools;

let

  nixBaseImage = pullImage {
    imageName = "nixos/nix";
    finalImageTag = "latest"; 
    imageDigest = "sha256:a383cd19ad51ffe1fd8717efd0bae81da9ed154c6183b64dffebe19a16a284ff";
    sha256 = "18imrdamgzj7fffky3hcnr17wv7zskz7d462mqxf7yvibhgyzvqw";
  };

  passwd = ''
    root:x:0:0::/root:/run/current-system/sw/bin/fish
    ${builtins.concatStringsSep "\n" (builtins.genList (i: "nixbld${toString (i+1)}:x:${toString (i+30001)}:30000::/var/empty:/run/current-system/sw/bin/nologin") 32)}
  '';

  group = ''
    root:x:0:
    nogroup:x:65534:
    nixbld:x:30000:${builtins.concatStringsSep "," (builtins.genList (i: "nixbld${toString (i+1)}") 32)}
  '';

  nsswitch = ''
    passwd:    files mymachines systemd
    group:     files mymachines systemd
    shadow:    files

    hosts:     files mymachines dns myhostname
    networks:  files

    ethers:    files
    services:  files
    protocols: files
    rpc:       files
  '';

  zenhaskell-user-environment = stdenv.mkDerivation {
    name = "zenhaskell-user-environment";
    phases = ["installPhase" "fixupPhase"];
    installPhase = ''
      mkdir -p $out/etc
      echo '${group}' > $out/etc/group
      echo '${passwd}' > $out/etc/passwd
      echo '${nsswitch}' > $out/etc/nsswitch.conf
    '';
  };

  stdConf = {
    Cmd = "fish";
    Env = [
            "ENV=/etc/profile"
            "FONTCONFIG_PATH=/etc/fonts"
            "GIT_SSL_CAINFO=${cacert}/etc/ssl/certs/ca-bundle.crt"
            "LANG=en_US.UTF-8"
            "LC_ALL=en_US.UTF-8"
            "LOCALE_ARCHIVE=${glibcLocales}/lib/locale/locale-archive"
            "NIX_PATH=/nix/var/nix/profiles/per-user/root/channels"
            "NIX_SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt"
            "PATH=/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin"
            "USER=root"
          ];
  };
in
rec {
  stackBaseImage = buildImage {
    name = "zenhaskell/foundation";
    tag = "latest";
    fromImage = nixBaseImage;

    contents = [
      bash
      cacert
      cachix
      cabal-install
      dhall
      fish
      ghc
      git
      glibcLocales
      hlint
      nix
      stack
      zenhaskell-user-environment
    ];

    config = stdConf;
  };
} 
