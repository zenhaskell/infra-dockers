# ZenHaskell Docker Infrastructure

This is the docker infrastructure for the
[https://zenhaskell.gitlab.io](ZenHaskell) template accelerator project.

There is one docker called `zenhaskell/foundation` which contains cabal, stack,
nix, bash and fish shells, ghc, hlint, cachix, git, dhall and some
configuration to allow stack and nix to work correctly as a gitlab-ci docker.

The docker is pushed to dockerhub
[here](https://hub.docker.com/r/zenhaskell/foundation).

# Building

Build the docker with

```
nix-build
```

You can load the docker into your docker image library with

```
docker load < result
```

# Updating Nix

```
nix-prefetch-git --quiet https://github.com/NixOS/nixpkgs | tee nixpkgs-src.json
```
